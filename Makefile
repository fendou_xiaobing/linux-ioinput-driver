obj-m += ioinput.o
KERNELDIR := /home/linuxuser/NUC972/linux-3.10.x #如果是用于arm平台，则内核路径为arm内核的路径
PWD = $(shell pwd)
all:
	make -C $(KERNELDIR) M=$(PWD) modules
clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions
