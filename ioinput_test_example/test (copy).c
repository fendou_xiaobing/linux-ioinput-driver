#include <stdio.h>  
#include <unistd.h>  
#include <stdlib.h>  
#include <string.h>  
#include <fcntl.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <linux/ioctl.h>  
#include <signal.h>  
#include <sys/select.h>
  
int main(int argc ,char *argv[])  
{  
	int fd;  
	int len,cnt;
	char buf[20];
	fd_set fds; 
	int retval; 
        struct timeval timeout={0,100}; //select等待100ms，100ms秒轮询

	fd = open("/dev/ioinput",O_RDWR);  
	if(fd < 0)  
	{         
	    	perror("open ioinput device");  
		return 0;  
	}  
	while(1)  
	{  
		
		printf("read .......100ms ....\n");
		usleep(100);
		len = read(fd, buf, 2);
		if(len > 0)
		{
			cnt = buf[0]+buf[1]*256;
			printf("%d\n",cnt);
		}


		FD_ZERO(&fds); 
		FD_SET(fd,&fds); 

		retval = select(fd+1,&fds,NULL,NULL,&timeout);
		if(retval < 0)
		{
			printf("Error\n");
		}
		else if(retval == 0)
		{
			printf("Time out\n");
		}
		else
		{
			len = read(fd, buf, 2);
			if(len > 0)
			{
				cnt = buf[0]+buf[1]*256;
				printf("%d\n",cnt);
			}
		}



	}  
	close(fd);                                                         
	return 0;  
}  
