#include <stdio.h>  
#include <unistd.h>  
#include <stdlib.h>  
#include <string.h>  
#include <fcntl.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <linux/ioctl.h>  
#include <signal.h>  
#include <sys/select.h>
#include<sys/poll.h> //poll()  
int main(int argc ,char *argv[])  
{  
	int fd;  
	int len,cnt;
	char buf[20];
	struct pollfd event; //创建一个struct pollfd结构体变量，存放文件描述符、要等待发生的事件
	int ret; 
     

	fd = open("/dev/ioinput",O_RDWR);  
	if(fd < 0)  
	{         
	    	perror("open ioinput device");  
		return 0;  
	}  
	while(1)  
	{  
		memset(&event,0,sizeof(event)); //memst函数对对象的内容设置为同一值
		event.fd=fd; //存放打开的文件描述符
		event.events=POLLIN; //存放要等待发生的事件

		ret=poll((struct pollfd *)&event,1,20); //监测event，一个对象，等待100ms毫秒后超时,-1为无限等待
		printf("read .......20ms ....\n");
		//判断poll的返回值，负数是出错，0是设定的时间超时，整数表示等待的时间发生
		if(ret<0){
		   printf("poll error!\n");
		   exit(1);
		  }

		  if(ret==0){
		   printf("Time out!\n");
		   continue;
		  }


		if(event.revents&POLLIN){
		  	len = read(fd, buf, 2);
		  	cnt = buf[0]+buf[1]*256;
			printf("%d\n",cnt);
		  }




	}  
	close(fd);                                                         
	return 0;  
}  
